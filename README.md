# Keyzer Server

A server that allows users to create groups for securely sharing passwords or secret files (e.g. certificates, private keys, etc.). These groups can be set up RBAC to configure who within a group has access to which object by role or individual.